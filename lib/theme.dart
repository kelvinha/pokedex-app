import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color secondaryColor = const Color(0xFFE3E3E4);

TextStyle textMontserrat = GoogleFonts.workSans(
  color: Colors.black,
  fontWeight: FontWeight.w400
);

TextStyle textMontserratBold = GoogleFonts.workSans(
  color: Colors.black,
  fontWeight: FontWeight.w700
);